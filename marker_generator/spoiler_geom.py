# -*- coding: utf-8 -*-
"""
Created on Thu Dec  6 17:28:41 2018

@author: Zoya
"""

import math
import numpy as np
import matplotlib.pyplot as plt

from skimage import data
from skimage import transform as tf
import random 

#text = data.text()


def persp_transform(img):
    
    coeff = [random.randint(-3,3) for i in range(8)]
    w, h = img.shape[:2]
    src = np.array([[0, 0], [0, h], [w, h], [w, 0]])
    dst = np.array([[0+coeff[0], 0+coeff[1]], [0+coeff[2], h+coeff[3]],
                    [w+coeff[4], h+coeff[5]], [w+coeff[6], 0+coeff[7]]])
    
    tform3 = tf.ProjectiveTransform()
    tform3.estimate(src, dst)
    warped = tf.warp(img, tform3, output_shape=(h, w))
    return warped


if __name__ == "__main__":
    
    img = plt.imread("marker_original\BASE1_100\mouse.png")    
    plt.imshow(persp_transform(img))
    plt.show()