# -*- coding: utf-8 -*-
"""
Created on Wed Dec 12 12:15:24 2018

@author: Zoya
"""

import matplotlib.pyplot as plt
import numpy as np
import os
from synthetic_marker import SyntheticMarker, SpoiledMarker
import glob

class SyntheticDatabase:
    
    def __init__(self, count, img, gtype = SyntheticMarker):
        
        self.count = count
        self.gtype = gtype
        
        if self.gtype == SyntheticMarker:
            self.img = np.mean(img, axis = 2)
            self.base = plt.imread(self.__get_calibrating_image())
        else:
            self.img = img
            
        self.markers = []
        self. __generate()    
           
    def __get_calibrating_image(self):
        """
        Returns path for calibrating image representing 2d square binarized 
        grid image
        :return string: 
        Path to file
        """
        pth = ""
        for token in __file__.split(os.sep)[:-1]:
            pth = os.path.join(pth,token)
        return os.path.join(pth,"marker_base\%d\marker318.png"%2)
    
    def __generate(self):
        
        if self.gtype.__name__ == SyntheticMarker.__name__:
            for i in range(self.count):
                self.markers.append(self.gtype(self.img, self.base, settings = ["CONTRAST", "GAUSS"], size = (100,100)))
        else:
            #print(self.gtype.__name__)
            for i in range(self.count):
                self.markers.append(self.gtype(self.img))
            
    def save(self, directory, additional_name=0):
        
        for i in range(self.count):
            plt.imsave("{0}//img{1}_{2}.png".format(directory, additional_name, i), self.markers[i].get())
            
            
if __name__ == "__main__":
    
        #SyntheticDatabase(20, plt.imread("marker_original\BASE1_100\star.png")).save("genma")
        
        orig_img = glob.glob("marker_base/all/*.png")
        for i in range(len(orig_img)):
            SyntheticDatabase(20, plt.imread(orig_img[i]), gtype = SpoiledMarker).save("marker_base/all", i)
            
        