# -*- coding: utf-8 -*-
"""
Created on Sun Apr  7 15:04:16 2019

@author: zoya
"""
#import glfw
from OpenGL.GL import *
from OpenGL.GLU import *
import OpenGL.GL.shaders
import numpy 
import pyrr
import ShaderLoader
from PIL import Image
import pygame
from pygame.locals import *



    
def create_cube(pos, indices):
    cube = [-0.1, -0.1,  0.1,  1.0, 1.0, 1.0,  0.0, 0.0,
             0.1, -0.1,  0.1,  1.0, 1.0, 1.0,  1.0, 0.0,
             0.1,  0.1,  0.1,  1.0, 1.0, 1.0,  1.0, 1.0,
            -0.1,  0.1,  0.1,  1.0, 1.0, 1.0,  0.0, 1.0,

            -0.1, -0.1, -0.1,  1.0, 1.0, 1.0,  0.0, 0.0,
             0.1, -0.1, -0.1,  1.0, 1.0, 1.0,  1.0, 0.0,
             0.1,  0.1, -0.1,  1.0, 1.0, 1.0,  1.0, 1.0,
            -0.1,  0.1, -0.1,  1.0, 1.0, 1.0,  0.0, 1.0,

             0.1, -0.1, -0.1,  1.0, 1.0, 1.0,  0.0, 0.0,
             0.1,  0.1, -0.1,  1.0, 1.0, 1.0,  1.0, 0.0,
             0.1,  0.1,  0.1,  1.0, 1.0, 1.0,  1.0, 1.0,
             0.1, -0.1,  0.1,  1.0, 1.0, 1.0,  0.0, 1.0,

            -0.1,  0.1, -0.1,  1.0, 1.0, 1.0,  0.0, 0.0,
            -0.1, -0.1, -0.1,  1.0, 1.0, 1.0,  1.0, 0.0,
            -0.1, -0.1,  0.1,  1.0, 1.0, 1.0,  1.0, 1.0,
            -0.1,  0.1,  0.1,  1.0, 1.0, 1.0,  0.0, 1.0,

            -0.1, -0.1, -0.1,  1.0, 1.0, 1.0,  0.0, 0.0,
             0.1, -0.1, -0.1,  1.0, 1.0, 1.0,  1.0, 0.0,
             0.1, -0.1,  0.1,  1.0, 1.0, 1.0,  1.0, 1.0,
            -0.1, -0.1,  0.1,  1.0, 1.0, 1.0,  0.0, 1.0,

             0.1,  0.1, -0.1,  1.0, 1.0, 1.0,  0.0, 0.0,
            -0.1,  0.1, -0.1,  1.0, 1.0, 1.0,  1.0, 0.0,
            -0.1,  0.1,  0.1,  1.0, 1.0, 1.0,  1.0, 1.0,
             0.1,  0.1,  0.1,  1.0, 1.0, 1.0,  0.0, 1.0]

    for i in range(0,len(cube),8):
        cube[i]-=0.1*pos[0]
        cube[i+1]-=0.1*pos[1]
        cube[i+2]-=0.1*pos[2]
    
    cube = numpy.array(cube, dtype = numpy.float32)

    #cube[:]

   

    vertex_shader = """
    #version 330
    in layout(location = 0) vec3 position;
    in layout(location = 1) vec3 color;
    in layout(location = 2) vec2 textureCoords;
    uniform mat4 transform;
    out vec3 newColor;
    out vec2 newTexture;
    void main()
    {
        gl_Position = transform * vec4(position, 1.0f);
        newColor = color;
        newTexture = textureCoords;
    }
    """
    
    fragment_shader = """
    #version 330
    in vec3 newColor;
    in vec2 newTexture;
    out vec4 outColor;
    uniform sampler2D samplerTexture;
    void main()
    {
        outColor = texture(samplerTexture, newTexture) * vec4(newColor, 1.0f);
    }
    """
    
    shader = OpenGL.GL.shaders.compileProgram(OpenGL.GL.shaders.compileShader(vertex_shader, GL_VERTEX_SHADER),
                                              OpenGL.GL.shaders.compileShader(fragment_shader, GL_FRAGMENT_SHADER))
    shader = ShaderLoader.compile_shader("shaders/video_18_vert.vs", "shaders/video_18_frag.fs")
    VBO = glGenBuffers(1)
    glBindBuffer(GL_ARRAY_BUFFER, VBO)
    glBufferData(GL_ARRAY_BUFFER, cube.itemsize * len(cube), cube, GL_STATIC_DRAW)

    EBO = glGenBuffers(1)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.itemsize * len(indices), indices, GL_STATIC_DRAW)

    #position
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, cube.itemsize * 8, ctypes.c_void_p(0))
    glEnableVertexAttribArray(0)
    #texture
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, cube.itemsize * 8, ctypes.c_void_p(24))
    glEnableVertexAttribArray(1)
    
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, cube.itemsize * 8, ctypes.c_void_p(24))
    glEnableVertexAttribArray(2)


    texture = glGenTextures(1)
    glBindTexture(GL_TEXTURE_2D, texture)
    # Set the texture wrapping parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT)
    # Set texture filtering parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
    # load image
    image = Image.open("res/output.jpg")
    img_data = numpy.array(list(image.getdata()), numpy.uint8)
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, image.width, image.height, 0, GL_RGB, GL_UNSIGNED_BYTE, img_data)
    #glEnable(GL_TEXTURE_2D)


    glUseProgram(shader)

   # glClearColor(0.2, 0.3, 0.2, 1.0)
    glEnable(GL_DEPTH_TEST)
    
    view = pyrr.matrix44.create_from_translation(pyrr.Vector3([0.0, 0.0, -3.0]))
    projection = pyrr.matrix44.create_perspective_projection_matrix(40.0, 1000 / 800, 0.1, 80.0)
    model = pyrr.matrix44.create_from_translation(pyrr.Vector3([0.0, 0.0, 0.0]))
    
    global view_loc
    global proj_loc
    global model_loc
    global transform_loc
    global light_loc
    
    view_loc = glGetUniformLocation(shader, "view")
    proj_loc = glGetUniformLocation(shader, "projection")
    model_loc = glGetUniformLocation(shader, "model")
    transform_loc = glGetUniformLocation(shader, "transform")
    light_loc = glGetUniformLocation(shader, "light")

    glUniformMatrix4fv(view_loc, 1, GL_FALSE, view)
    glUniformMatrix4fv(proj_loc, 1, GL_FALSE, projection)
    glUniformMatrix4fv(model_loc, 1, GL_FALSE, model)
    
    return shader
    #glPolygonMode(GL_FRONT_AND_BACK, GL_LINE)

    #while not glfw.window_should_close(window):
       # glfw.poll_events()
def main():
    
    indices = [ 0,  1,  2,  2,  3,  0,
            4,  5,  6,  6,  7,  4,
            8,  9, 10, 10, 11,  8,
           12, 13, 14, 14, 15, 12,
           16, 17, 18, 18, 19, 16,
           20, 21, 22, 22, 23, 20]

    indices = numpy.array(indices, dtype= numpy.uint32)
    pygame.init()
    display = (1000, 800)
    pygame.display.set_mode(display, DOUBLEBUF|OPENGL)
   # glLoadIdentity();
    
    
    glTranslatef(0.0,0.0, -1)
    shader = create_cube([0,0,0], indices)
   # indices += 1
   # shader1 = create_cube(-3, indices2)
    pos_x = list(range(-10, 10, 2))
    pos_y = list(range(-8, 8, 2))
    
    position = []
    
    for x in pos_x:
        for y in pos_y:
            position.append([x,y,1])
    i = 0   
    j = 0     
    
   
    
    while True:
        
        print(projection)
        if j%30 == 0:
            pos = position[i%len(position)]
            i+=1
        j+=1
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
        
        shader = create_cube(pos, indices)
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        
        
        rot_x = pyrr.Matrix44.from_x_rotation(0.8)
        rot_y = pyrr.Matrix44.from_y_rotation(0.2)
#
        transformLoc = glGetUniformLocation(shader, "transform")
       # glUniformMatrix4fv(transformLoc, 1, GL_FALSE, rot_x * rot_y)
       
        
        glUniformMatrix4fv(transform_loc, 1, GL_FALSE, rot_y)
        glUniformMatrix4fv(light_loc, 1, GL_FALSE, rot_x*rot_y)
        
        
        glDrawElements(GL_TRIANGLES, len(indices), GL_UNSIGNED_INT, None)
        
      

      #  transformLoc = glGetUniformLocation(shader1, "transform")
     #   glUniformMatrix4fv(transformLoc, 1, GL_FALSE, rot_x * rot_y)
     #   glDrawElements(GL_TRIANGLES, len(indices2), GL_UNSIGNED_INT, None)
        
        pygame.display.flip()
        pygame.time.wait(10)

      #  glfw.swap_buffers(window)

  #  glfw.terminate()

if __name__ == "__main__":
    main()
