# -*- coding: utf-8 -*-
"""
Created on Thu Dec  6 15:46:59 2018

@author: Zoya
"""

import numpy as np
from  matplotlib.colors import rgb_to_hsv, hsv_to_rgb
import matplotlib.pyplot as plt
from skimage.filters import threshold_otsu, threshold_local
import sys
import random
from PIL import Image

from phone_generator import color_range, fond_creator
from spoiler_filters import change_contrast, sharpness, gauss
from spoiler_geom import persp_transform

print()

class SyntheticMarker(object):
    
    GEN_SETTINGS = ((14, 26), (1,2), (2,5))
    def __init__(self,img, base):
        # img - двухмерная картинка ОШИБКА
        self.base = base
        self.orig_img = img
        self.img = self.__synthesize(persp_transform(img))     
       

    
    def get(self):
        return self.img
    
    def __synthesize(self, img):
        
        crange = color_range(rgb_to_hsv(self.base))
        gray_bw_img = np.mean(self.base, axis = 2)         
        mask1 = np.where(gray_bw_img)        
        fond = fond_creator((100,100,3), crange[-1300:], mask1)  
        
        global_thresh = threshold_otsu(img)
        binary_global = img > global_thresh
        mask2 = np.where(binary_global == False)        
        genimg = fond_creator((100,100,3), crange[:1600], mask2, fond) 
        
        img = genimg.copy()
        filters = [change_contrast, gauss, sharpness]
        E = random.randint(2, 5)
        for i in range(E):
            i = random.randint(0, len(filters)-1)
            fi = filters[i]
            p = random.randint(self.GEN_SETTINGS[i][0],
                               self.GEN_SETTINGS[i][1])
#            print(i, end = " ")
            img = fi(img, p)
             
        return img

 
#img = Image.open("marker_original\BASE1_100\mouse.png")
#
#base = Image.open("marker_base\\2\marker318.png")
if __name__ == "__main__":
    base = plt.imread("marker_base\\2\marker318.png")
    img = plt.imread("marker_original\BASE1_100\mouse.png")
    img = np.mean(img, axis = 2)
    marker = SyntheticMarker(img, base)
    
    plt.imshow(marker.get())
    plt.show()
