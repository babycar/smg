# -*- coding: utf-8 -*-
"""
Created on Wed Dec  5 19:52:35 2018

@author: Zoya
"""

from PIL import Image, ImageFilter, ImageEnhance
import numpy as np
import matplotlib.pyplot as plt
from  matplotlib.colors import rgb_to_hsv, hsv_to_rgb
from random import randint, choice
import matplotlib.pyplot as plt


def color_range(img):
    all_colors = img.reshape(-1,3)
    colors = np.unique(all_colors, axis = 0).tolist()
    colors = sorted(colors, key = lambda lst: lst[2])
    return colors

def fond_creator(size, color_range, mask, base = 0):
    color_range = np.asarray(color_range)
    color_range = hsv_to_rgb(color_range)
    if type(base)!=int:
        img = np.array(base.copy())
    else:
        img = np.zeros(size)      
    
    for i,j in list(zip(*mask)):
        pixel = np.rint((choice((color_range)) * 255)) 
        img[i][j][:] = pixel
        
    return Image.fromarray(img.astype('uint8'))

  