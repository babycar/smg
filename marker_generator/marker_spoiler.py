# -*- coding: utf-8 -*-
"""
Created on Fri Nov 30 17:49:06 2018

@author: Zoya
"""

#гауссина, белый шум. 
#Изменение контраста, резкости

from PIL import Image, ImageFilter, ImageEnhance
import numpy as np
from random import randint
import matplotlib.pyplot as plt


def plot_effects(orig_img, spoil_img, effect_name = "effects_plots\\effect"):     
    plt.figure(figsize = (10, 7))   
    plt.subplot(121)
    plt.title("Original", fontsize=20)
    plt.imshow(orig_img)
    plt.subplot(122)
    plt.title(effect_name, fontsize=20)
    plt.imshow(spoil_img)
    plt.tight_layout()
    plt.savefig(effect_name+".png")
    plt.show()

def change_contrast(img, level):
    factor = (259 * (level + 255)) / (255 * (259 - level))
    def contrast(c):
        value = 128 + factor * (c - 128)
        return max(0, min(255, value))
    return img.point(contrast)

def sharpness(img, factor):
    img = ImageEnhance.Sharpness(img).enhance(factor)
    return img

def gauss(img, level):
    return img.filter(ImageFilter.GaussianBlur(level))

def white_noise(img, R):
    img = img.copy()
    r = 0;
    width, height = img.size
    width-=1
    height-=1
    while r!=R:
        pixel = (randint(0,255), randint(0,255), randint(0,255))
        coord = (randint(0, width), randint(0, height) )
        img.putpixel(coord, pixel)
        r+=1
    return img

if __name__ == "__main__":
    
    orig_img = Image.open('marker_base\\2\\marker53.png')
    spoil_img1 = change_contrast(orig_img, 34)
    spoil_img2 = gauss(orig_img, 2)
    spoil_img3 = sharpness(orig_img, 6)
    spoil_img4 = white_noise(orig_img, 1000)    
    npimg = np.asarray(spoil_img1)    
    plot_effects(orig_img, spoil_img1, "effects_plots\\contrast")  
    plot_effects(orig_img, spoil_img2, "effects_plots\\gaus") 
    plot_effects(orig_img, spoil_img3, "effects_plots\\sharpness")
    plot_effects(orig_img, spoil_img4, "effects_plots\\white_noise")
    
    
