# -*- coding: utf-8 -*-
"""
Created on Thu Dec  6 17:47:02 2018

@author: Zoya
"""

from PIL import Image, ImageFilter, ImageEnhance
import numpy as np
from  matplotlib.colors import rgb_to_hsv, hsv_to_rgb
import matplotlib.pyplot as plt
from skimage.filters import threshold_otsu, threshold_local
import sys

sys.path.append('../../marker_generator/')

from synthetic_marker import SyntheticMarker


base = plt.imread("marker318.png")
img = plt.imread("mouse.png")
img = np.mean(img, axis = 2)
marker = SyntheticMarker(img, base)

plt.imshow(marker.get())
plt.show()