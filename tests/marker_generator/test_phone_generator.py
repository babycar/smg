# -*- coding: utf-8 -*-
"""
Created on Wed Dec  5 19:56:46 2018

@author: Zoya
"""

import numpy as np
from  matplotlib.colors import rgb_to_hsv, hsv_to_rgb
import matplotlib.pyplot as plt
from skimage.filters import threshold_otsu, threshold_local
import sys

sys.path.append('../../')

from marker_generator.phone_generator import color_range, fond_creator

np_orig_img = plt.imread('marker318.png')
color_range = color_range(rgb_to_hsv(np_orig_img))
gray_bw_img = np.mean(np_orig_img, axis = 2)
 
mask1 = np.where(gray_bw_img)

fond = fond_creator((100,100,3), color_range[-1300:], mask1)  

bw_img = plt.imread('mouse.png')
gray_bw_img = np.mean(bw_img, axis = 2)
global_thresh = threshold_otsu(gray_bw_img)
binary_global = gray_bw_img > global_thresh
mask2 = np.where(binary_global == False)

genimg = fond_creator((100,100,3), color_range[:1600], mask2, fond)

plt.figure(figsize = (10, 7))  
plt.subplot(121)
plt.title("FOND", fontsize=20)
plt.imshow(fond)

plt.subplot(122)
plt.title("PICTURED", fontsize=20)
plt.imshow(genimg)
plt.tight_layout()

plt.show()


