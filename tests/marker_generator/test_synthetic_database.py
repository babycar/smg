# -*- coding: utf-8 -*-
"""
Created on Wed Dec 12 12:24:33 2018

@author: Zoya
"""


from PIL import Image, ImageFilter, ImageEnhance
import numpy as np
from  matplotlib.colors import rgb_to_hsv, hsv_to_rgb
import matplotlib.pyplot as plt
from skimage.filters import threshold_otsu, threshold_local
import sys

sys.path.append('../../marker_generator/')
from synthetic_database import SyntheticDatabase

img = plt.imread("star.png")
markers = SyntheticDatabase(100, img)
markers.save("genma/star")

