# -*- coding: utf-8 -*-
"""
Created on Wed Dec  5 20:14:00 2018

@author: Zoya
"""

from PIL import Image, ImageFilter, ImageEnhance
import numpy as np
from  matplotlib.colors import rgb_to_hsv, hsv_to_rgb
import matplotlib.pyplot as plt
from skimage.filters import threshold_otsu, threshold_local
import sys

sys.path.append('../../')

from marker_generator.spoiler_filters import (change_contrast,
                                                          gauss,
                                                          sharpness,
                                                          white_noise,
                                                          plot_effects)

orig_img = Image.open('marker318.png')   
spoil_img1 = change_contrast(orig_img, 34)
spoil_img2 = gauss(orig_img, 2)
spoil_img3 = sharpness(orig_img, 6)
spoil_img4 = white_noise(orig_img, 1000)    
npimg = np.asarray(spoil_img1)    

plot_effects(orig_img, spoil_img1)  
plot_effects(orig_img, spoil_img2) 
plot_effects(orig_img, spoil_img3)
plot_effects(orig_img, spoil_img4)


